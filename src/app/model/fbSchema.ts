export class FbSchema {
  public value: string;
  public key: string;
  public label: string;
  public required: boolean;
  public controlType: string;
  public listing?: string[];
}
