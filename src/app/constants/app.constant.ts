const STATES: string[] = ['Tamil Nadu', 'Karnataka', 'Kerala'];
const INPUT_TYPES: any[] = [
    { value: 'text', viewValue: 'Text' },
    { value: 'number', viewValue: 'Number' },
    { value: 'checkbox', viewValue: 'Check Box' },
    { value: 'radio', viewValue: 'Radio' },
    { value: 'dropdown', viewValue: 'Dropdown' }
  ];

const  CONDITIONS: any[] = [
    { value: 'maxLen', viewValue: 'Maximum Length' },
    { value: 'minLen', viewValue: 'Minimum Length' },
    { value: 'lenBtw', viewValue: 'Length Between' },
    { value: 'regx', viewValue: 'Regular Expression' },
  ];

const MOCK_SCHEMA: any[] =  [{
    value: '',
    key: 'name',
    label: 'Name',
    required: true,
    controlType: 'text'
  },
  {
    value: '',
    key: 'amount',
    label: 'Amount',
    required: true,
    controlType: 'number'
  },
  {
    value: '',
    key: 'state',
    label: 'state',
    required: true,
    controlType: 'dropdown',
    listing: STATES
  },
  {
    value: '',
    key: 'gender',
    label: 'Gender',
    required: true,
    controlType: 'radio',
    listing: ['Male', 'Female']
  },
  {
    value: '',
    key: 'option',
    label: 'option',
    required: true,
    controlType: 'checkbox',
    listing: ['one', 'two']
  }

  ];

export const AppConstant: any = {
    INPUT_TYPES : INPUT_TYPES,
    CONDITIONS : CONDITIONS,
    MOCK_SCHEMA : MOCK_SCHEMA
};


