import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatCardModule, MatSlideToggleModule,
  MatSelectModule, MatIconModule, MatRadioModule, MatExpansionModule, MatListModule } from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule, MatCheckboxModule, MatInputModule, MatCardModule, MatSlideToggleModule, MatSelectModule,
    MatIconModule, MatRadioModule, MatExpansionModule, MatListModule
  ],
  exports: [
    MatButtonModule, MatCheckboxModule, MatInputModule, MatCardModule, MatSlideToggleModule, MatSelectModule,
    MatIconModule, MatRadioModule, MatExpansionModule, MatListModule
  ]
})
export class CustomMaterialModuleModule { }
