import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderComponent } from './form-builder.component';
import { FormRendererComponent } from './form-renderer/form-renderer.component';
import { DynamicFormBuilderService } from './services/dynamic-form-builder.service';
import { CustomMaterialModuleModule } from '../custom-material-module/custom-material-module.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CustomMaterialModuleModule
  ],
  declarations: [
    FormBuilderComponent,
    FormRendererComponent
  ],
  providers: [DynamicFormBuilderService]
})
export class FormBuilderModule { }
