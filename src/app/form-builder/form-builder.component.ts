import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicFormBuilderService } from './services/dynamic-form-builder.service';
import { FbSchema } from '../model/fbSchema';
import { AppConstant as DEFAULT } from '../constants/app.constant';
@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.css']
})
export class FormBuilderComponent implements OnInit {

  constructor(private fb: FormBuilder, public _dFbService: DynamicFormBuilderService) { }

  heroForm;
  dynaForm;
  states: string[] = DEFAULT.STATES;
  types: any[] = DEFAULT.INPUT_TYPES;
  conditions: any[] = DEFAULT.CONDITIONS;

  selectedValue: String = 'Text';
  schema: FbSchema[] = DEFAULT.MOCK_SCHEMA;

  ngOnInit() {
    this.createForm();
    this.dynaForm = this._dFbService.toFormGroup(this.schema);
  }

  valChan(checked) {
    console.log(checked);
  }

  createForm() {
    this.heroForm = this.fb.group({
      name: ['', Validators.required], // <--- the FormControl called "name"
      state: ['', Validators.required],
      power: ['', Validators.required],
      sidekick: ['', Validators.required]
    });
  }

  onSubmit() {
    console.log(this.dynaForm.value);
  }

  addElement() {
    let newSchema = new FbSchema();
    newSchema.key = 'obj' + (this.schema.length - 1);
    newSchema.label = 'Object' + (this.schema.length - 1);
    newSchema.required = false;
    newSchema.controlType = 'text';
    this.schema.push(newSchema);
    console.log(this.schema);
  }

  deleteElement(field) {
    let index = this.schema.findIndex((item) => {
      return item.key === field.key;
    });
    if (index > -1) {
      this.schema.splice(index, 1);
    }
  }

  addField(element, field) {
    console.log(element.name);
    if (field.listing === undefined || field.listing.length <= 0) {
      field.listing = [];
    }
    if (element.value) {
      field.listing.push(element.value);
      element.value = '';
    }
    console.log(field);
  }

  deleteField(value, list) {
    this.removeFromArray(value, list);
  }

  removeFromArray(search, array) {
    let index = array.indexOf(search);
    console.log(index);
    if (index > -1) {
      array.splice(index, 1);
    }
  }



}
