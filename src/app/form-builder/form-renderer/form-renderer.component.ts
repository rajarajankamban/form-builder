import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FbSchema } from '../../model/fbSchema';

@Component({
  selector: 'app-form-renderer',
  templateUrl: './form-renderer.component.html',
  styleUrls: ['./form-renderer.component.css']
})
export class FormRendererComponent implements OnInit, OnChanges {

  constructor() { }

  @Input() element: FbSchema;
  @Input() form: FormGroup;

  ngOnInit() {
    console.log(this.form);
    console.log(this.element);
  }
  ngOnChanges() {
    console.log(this.form);
  }
  // get isValid() { return this.form.controls[this.element.key].valid; }

  isPristine(key) {
    // console.log('Pristine:' + this.form.controls[key].pristine);
    return true;
  }
  isValid(key) {
    console.log('isValid:' + this.form.controls[key].valid);
    return this.form.controls[key].valid;
  }
  isRequired(key) {
    console.log('isRequired:' + this.form.controls[key].errors);
    return this.form.controls[key].errors ? this.form.controls[key].errors.required : false;
  }
}
