import { Injectable } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { FbSchema } from '../../model/fbSchema';
@Injectable()
export class DynamicFormBuilderService {

  constructor(private fb: FormBuilder) { }

  toFormGroup(fbSchema: FbSchema[]) {
    const group: any = {};

    fbSchema.forEach(element => {
      group[element.key] = element.required ? new FormControl(element.value || '', Validators.required)
        : new FormControl(element.value || '');
    });
    return new FormGroup(group);
  }

  // createFormElement(fbSchema: FbSchema[]) {
  //   this.fb.group({
  //     name: ['', Validators.required], // <--- the FormControl called "name"
  //     state: ['', Validators.required],
  //     power: ['', Validators.required],
  //     sidekick: ['', Validators.required]
  //   });
  // }
}



// {
// 	"value":"" ,
// 	"key":"amount",
// 	"label": "Amount",
// 	"required": true,
// 	"controlType": "shortText",
// }

